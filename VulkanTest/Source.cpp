#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>

class helloWorld
{
public:
	void run() 
	{
		initWindow();
		initVulkan();
		mainLoop();
		cleanUp();
	}

private:
	GLFWwindow * mainWindow;
	const int HEIGHT = 600;
	const int WIDTH = 800;
	VkInstance instance;
	VkDebugReportCallbackEXT callback;
	
	const std::vector<const char*> validationLayers = 
	{
		"VK_LAYER_LUNARG_standard_validation"
	};

#ifdef NDEBUG
	const bool enableValidationLayers = false;
#else
	const bool enableValidationLayers = true;
#endif // NDEBUG

	bool checkValidationLayerSupport()
	{
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties>availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers)
		{
			bool layerFound = false;
			for (const auto& layerProperties : availableLayers)
			{
				if (strcmp(layerName, layerProperties.layerName) == 0)
				{
					layerFound = true;
					break;
				}
			}
			if (!layerFound)
			{
				return false;
			}
		}
		return true;
	}

	std::vector<const char*> getRequiredExtentions()
	{
		uint32_t glfwExtentionCount = 0;
		const char** glfwExtentions;
		glfwExtentions = glfwGetRequiredInstanceExtensions(&glfwExtentionCount);

		std::vector<const char*> extentions(glfwExtentions, glfwExtentions + glfwExtentionCount);
		if (enableValidationLayers)
		{
			extentions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		}
		return extentions;
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallBack(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData)
	{
		std::cerr << "validation layer: " << msg << std::endl;
		return VK_FALSE;
	}

	void initWindow()
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE,GLFW_FALSE);
		mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "test", nullptr, nullptr);
	}
	void initVulkan()
	{
		createInstance();
		setupDebugCallback();
	}
	void setupDebugCallback()
	{
		if (!enableValidationLayers)
			return;
	}
	void mainLoop()
	{
		while (!glfwWindowShouldClose(mainWindow))
		{
			glfwPollEvents();
		}
	}
	void cleanUp()
	{
		glfwDestroyWindow(mainWindow);

		glfwTerminate();
	}
	void createInstance()
	{
		if (enableValidationLayers && !checkValidationLayerSupport())
		{
			throw std::runtime_error("validation layers requested, but not available");
		}
		else
		{
			VkApplicationInfo appInfo = {};
			appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
			appInfo.pApplicationName = "Triangle";
			appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
			appInfo.pEngineName = "No Engine";
			appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
			appInfo.apiVersion = VK_API_VERSION_1_0;

			VkInstanceCreateInfo createInfo = {};
			if (enableValidationLayers)
			{
				createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
				createInfo.ppEnabledLayerNames = validationLayers.data();
			}
			else
			{
				createInfo.enabledLayerCount = 0;
			}
			createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
			createInfo.pApplicationInfo = &appInfo;

			auto extentions = getRequiredExtentions();
			createInfo.enabledExtensionCount = static_cast<uint32_t>(extentions.size());
			createInfo.ppEnabledExtensionNames = extentions.data();

			uint32_t glfwExtentionCount = 0;
			const char** glfwExtentions;

			glfwExtentions = glfwGetRequiredInstanceExtensions(&glfwExtentionCount);

			createInfo.enabledExtensionCount = glfwExtentionCount;
			createInfo.ppEnabledExtensionNames = glfwExtentions;

			createInfo.enabledLayerCount = 0;

			VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);
		}
	}
};

int main() {
	helloWorld *app = new helloWorld;
	
	try
	{
		app->run();
	}
	catch (const std::runtime_error& e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
		delete app;
	}
	delete app;
	return EXIT_SUCCESS;
}